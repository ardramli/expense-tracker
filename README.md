# Expense Tracker App

A simple expense tracker app built using Flutter. Integrated with Google Sheets API.

- [Invite Link to Google Spreadsheet](https://docs.google.com/spreadsheets/d/17uC2a3gWn61a2XlgHXTsFJr1W9XygTD_tNgFqErukQs/edit?usp=sharing)

## Steps to install

- [How to install Flutter](https://docs.flutter.dev/get-started/install)
- Clone project
- Open project in [Visual Studio Code](https://docs.flutter.dev/development/tools/vs-code). Make sure to install Flutter and Dart extensions. Or you can use [Android Studio](https://docs.flutter.dev/development/tools/android-studio) too.
- Run the App.
