// ignore_for_file: prefer_const_declarations, unused_field

import 'package:gsheets/gsheets.dart';

class GoogleSheetsAPI {
  //c create credentials
  static const _credentials = r'''
  {
  "type": "service_account",
  "project_id": "expense-tracker-339416",
  "private_key_id": "b1e0f9865f1601d226a16cfc7b4817542a583494",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC+px6tzgVPMHRQ\nQDJ5qc+nowjXq7kCgmibKUN4pJM+WCEdvuXOIhgQyll/wulvhhYTN4EkYqB+MEem\n3TEsplfI3q0mcAIpRrMfEUFySOhZ0BWsCDGXvixNxspd6tbkTNuTKuttz0+cB6FJ\nJb845OninjUnNgXABM2WWvI5HmbtTyjb7izlsF/DvrMVHqonEgum7xWHnRtWOA+X\nzSMc+2KG4JitfM5Pkojfsn5+ZpbuwMMHRbhrf03fmamvNO//DeFGrdDg12XFcBrr\nheGbldfD3xEtMFyh5Xo1YdeGGp1Q0CEFeEdaYLsy/gqFkdOnSUAuoVw5j/Unn4h5\nHOT/Qc1NAgMBAAECggEABg1nH2xhZcSrhJqtH4ihA3jdAH++lVJPgNF8B1nuJLYG\ngxu6zuLtMadC1Rf6ciRnEgAalO2em7Lrq2PtOjYjpkhPZd97ahUI/az+D2M6IRPG\nhFoUPPV4EZYEeZvJa6D2/YKIy8kfhVwGpjxRCDzYIe4U0ORZpWFhp2rGssKA0Pq6\nzsw3l7mgOFSAq3vCZU2Fw825hsPvA57ly6yHXHeYUjsA3XHhEC2IbCV38yKdHTAA\n6vpMVZWCTIB2CnEEZHZOfW0hMf6D2It3UE4jYMV2IXjDnKdn5/CIYNSn3trCsvCK\nI1/W5W+9sGNuSsMNJUvVXtQeGl6uWKpH3yOfWuqvoQKBgQDmZXdh+DeuGfjlyazk\n5n6bhKhpgwxGxOXZiIvK6xFgHAYyQERJZrztgE1O/9nSSz73yp56E6uDWMU1x8u7\nSAf4xhH1NqU0cda9lKHlmYpxJeGge/ihFWt4+P/o+XRBA0z98Nr5sZi+zbdUyRqR\nlW0I6PDJ28rdlXtQhRGUNjY1XQKBgQDT1v3Zww4EyX3RueNbq4OePIxGsIeBgQc5\nX4D0pnIAt1lACtB7TgdtwvQ3W0n7+RJsTuY0k0dJFF0nMie0Igy+RLtIHaZuOkZD\nd1HIF9f6pr1CvoEvO3Ua+DXLawqy9wrJgzydymryWfDsd/Kf2cDRPvwxWFIjpf98\nAiJDvMwIsQKBgQDFFyGkNlFzZxZrhDWZ7q953GaxNADJfWBe0Zc8dBB+Fhsa261O\n2UtfJHV90hKE+geYs8UUC0Waa9LwacUXpcvj/ZSZHzD+BzA7Jy3DmrA6kLxNrB+J\ntSgl6WQJ6XOQtgEj7ulz1oAjIRnmts46oggPKi3pkFMfcON3gw4Bqsp+pQKBgHpB\nMxM/jJGarzAgNWQsIbNqghIxwqIskry7rzKB9H0RU0Kwew0ORR9FSafA2Xu/jLBR\nAG4LKTK1u+To/xzoIa6U//holCo9G1vIOrxddlnDbKpeQzO8/qJn6LPSvMP36pPN\nV2hA72OaIH534t/5zVt1HBpmPFEydbVdjeTFyoKhAoGASgc7g0NIsBq5grbFCx6u\nqjikwXeTmRKXrInI6+9k6l7E4ljACHHo08U4W0pivSdiJeZqE3ZzkuJ4dPSVW/sa\nXHcy5QugjqVuZUAPBeBiJus4pNnwWNKTUEt18NCkcJ+KpxXWAl7ckWpfoLovMFcr\nv/Uvd5CQSFVqGyfx4I6Pwsc=\n-----END PRIVATE KEY-----\n",
  "client_email": "expense-tracker@expense-tracker-339416.iam.gserviceaccount.com",
  "client_id": "102373232117908686542",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/expense-tracker%40expense-tracker-339416.iam.gserviceaccount.com"
}
  ''';

  //setup and connect to the spreadsheet
  static final _spreadsheetId = '17uC2a3gWn61a2XlgHXTsFJr1W9XygTD_tNgFqErukQs';
  static final _gsheets = GSheets(_credentials);
  static Worksheet? _worksheet;

  // some variables to keep track of..
  static int numberOfTransactions = 0;
  static List<List<dynamic>> currentTransactions = [];
  static bool loading = true;

  // initialize the spreadsheet
  Future init() async {
    final ss = await _gsheets.spreadsheet(_spreadsheetId);
    _worksheet = ss.worksheetByTitle('Worksheet1');
    countRows();
  }

  // count the number of notes
  static Future countRows() async {
    while ((await _worksheet!.values
            .value(column: 1, row: numberOfTransactions + 1)) !=
        '') {
      numberOfTransactions++;
    }
    // now we know how many notes to load, now let's load them!
    loadTransactions();
  }

  // load existing notes from the spreadsheet
  static Future loadTransactions() async {
    if (_worksheet == null) return;

    for (int i = 1; i < numberOfTransactions; i++) {
      final String transactionName =
          await _worksheet!.values.value(column: 1, row: i + 1);
      final String transactionAmount =
          await _worksheet!.values.value(column: 2, row: i + 1);
      final String transactionType =
          await _worksheet!.values.value(column: 3, row: i + 1);

      if (currentTransactions.length < numberOfTransactions) {
        currentTransactions.add([
          transactionName,
          transactionAmount,
          transactionType,
        ]);
      }
    }
    print(currentTransactions);
    // this will stop the circular loading indicator
    loading = false;
  }

  // insert a new transaction
  static Future insert(String name, String amount, bool _isIncome) async {
    if (_worksheet == null) return;
    numberOfTransactions++;
    currentTransactions.add([
      name,
      amount,
      _isIncome == true ? 'income' : 'expense',
    ]);
    await _worksheet!.values.appendRow([
      name,
      amount,
      _isIncome == true ? 'income' : 'expense',
    ]);
  }

  // CALCULATE THE TOTAL INCOME!
  static double calculateIncome() {
    double totalIncome = 0;
    for (int i = 0; i < currentTransactions.length; i++) {
      if (currentTransactions[i][2] == 'income') {
        totalIncome += double.parse(currentTransactions[i][1]);
      }
    }
    return totalIncome;
  }

  // CALCULATE THE TOTAL EXPENSE!
  static double calculateExpense() {
    double totalExpense = 0;
    for (int i = 0; i < currentTransactions.length; i++) {
      if (currentTransactions[i][2] == 'expense') {
        totalExpense += double.parse(currentTransactions[i][1]);
      }
    }
    return totalExpense;
  }
}
